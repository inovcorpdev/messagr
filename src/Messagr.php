<?php

namespace Inovcorp\Messagr;

use Inovcorp\Greetr\Greetr;

class Messagr
{
    /**
     * @param string $sName The name to display on the message
     * @param int $type The type of message to display. 1: Greeting | 2: Goodbye
     * @return string
     */
    public function message(string $sName, $type = 1): string
    {
        $sGreetr = new Greetr();
        if ($type == 1) {
            return $sGreetr->greet($sName);
        }
        else if ($type == 2) {
            return $sGreetr->goodbye($sName);
        }
        else {
            return 'Nothing to show';
        }
    }
}
